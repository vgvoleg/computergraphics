package com.vgvoleg.imageredactor.utils;

/**
 * Created by olov0816 on 05.10.2016.
 */
public class MatrixOp
{
    private static final double[][] rgbToLmsMatrix = new double[][]{
            {0.3811, 0.5783, 0.0402},
            {0.1967, 0.7244, 0.0782},
            {0.0241, 0.1288, 0.8444}
    };

    private static final double[][] lmsLab1 = new double[][]{
            {0.5774, 0, 0},
            {0, 0.4082, 0},
            {0, 0, 0.7071}
    };

    private static final double[][] lmsLab2 = new double[][]{
            {1, 1, 1},
            {1, 1, -2},
            {1, -1, 0}
    };

    private static final double[][] lmsToRGB = new double[][]{
            {4.4679, -3.5873, 0.1193},
            {-1.2186, 2.3809, -0.1624},
            {0.0497, -0.2439, 1.2045}
    };

    private static double[][] multMatrix(double[][] mA, double[][] mB){
        int m = mA.length;
        int n = mB[0].length;
        int o = mB.length;
        double[][] res = new double[m][n];

        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                res[i][j] = 0;
                for (int k = 0; k < o; k++) {
                    res[i][j] += mA[i][k] * mB[k][j];
                }
            }
        }
        return res;
    }


    public static void printMatrix(double[][] matrix){
        for (int i = 0; i< matrix.length; i++){
            for (int j = 0; j<matrix[0].length; j++){
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
        System.out.println("--------------------");
    }


    public static double[][] convertToLab(int red, int green, int blue){
        double[][] rgbMatrix = new double[][]{
                {Utils.normColor(red)},
                {Utils.normColor(green)},
                {Utils.normColor(blue)}
        };

        double[][] lmsMatrix = multMatrix(rgbToLmsMatrix, rgbMatrix);
        lmsMatrix[0][0] = Math.log10(lmsMatrix[0][0]);
        lmsMatrix[1][0] = Math.log10(lmsMatrix[1][0]);
        lmsMatrix[2][0] = Math.log10(lmsMatrix[2][0]);
        double[][] labMatrix = multMatrix(multMatrix(lmsLab1, lmsLab2), lmsMatrix);
        return labMatrix;
    }

    public static int[][] convertToRGB(double [][] labMatrix){
        double[][] lmsMatrix = multMatrix(multMatrix(lmsLab2, lmsLab1), labMatrix);
        lmsMatrix[0][0] = Math.pow(10, lmsMatrix[0][0]);
        lmsMatrix[1][0] = Math.pow(10, lmsMatrix[1][0]);
        lmsMatrix[2][0] = Math.pow(10, lmsMatrix[2][0]);

        double[][] rgbMatrixTemp = multMatrix(lmsToRGB, lmsMatrix);
        int[][] rgbMatrix = new int[][]{
                {(int)(rgbMatrixTemp[0][0]*255)},
                {(int)(rgbMatrixTemp[1][0]*255)},
                {(int)(rgbMatrixTemp[2][0]*255)}
        };
        return rgbMatrix;
    }
}
