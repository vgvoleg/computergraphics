package com.vgvoleg.imageredactor.utils;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 21.09.2016.
 */
public class Utils
{
    public static BufferedImage getBufferedImage(Image img){
        return SwingFXUtils.fromFXImage(img, null);
    }

    public static Image setImage(BufferedImage img) {
        return SwingFXUtils.toFXImage(img, null);
    }

    public static int getCorrectColorValue(int value){
        if (value > 255){
            return 255;
        } else if (value < 0){
            return 0;
        } else {
            return value;
        }
    }

    public static double normColor(int color){
        double d = color*235.0/(255*255);
        if (d < 0.01179){
            d = 0.01179;
        }
        return d;
    }
}
