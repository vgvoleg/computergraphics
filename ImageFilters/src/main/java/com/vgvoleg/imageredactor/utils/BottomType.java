package com.vgvoleg.imageredactor.utils;

/**
 * Created by olov0816 on 04.10.2016.
 */
public enum BottomType
{
    SLIDER_SEPIA,
    SLIDER_BRIGHTNESS,
    SLIDER_LIGHTNING,
    INPUT_COLOR,
    INPUT_IMAGE
}
