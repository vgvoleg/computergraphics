package com.vgvoleg.imageredactor.utils;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.util.Stack;

/**
 * Created by Олег on 03.10.2016.
 */
public class ImageManager
{
    private ImageView imageView;
    private Stack<Image> stack;
    private Image temp = null;

    public ImageManager(ImageView imageView) {
        this.imageView = imageView;
        stack = new Stack();
    }

    private void refreshImage(){
        imageView.setImage(top());
    }

    public void saveTemp(){
        temp = null;
    }

    public boolean haveTemp(){
        return (temp != null);
    }

    public void setTemp(Image img){
            temp = img;
            add(temp);
    }

    public void setNewImage(Image img){
        stack = new Stack();
        temp = null;
        add(img);
    }

    public void reset(){
        while (stack.size() > 1){
            stack.pop();
        }
        temp = null;
        refreshImage();
    }

    public Image top(){
        if (!stack.empty()) {
            return stack.peek();
        }
        return null;
    }

    public void pop(){
        if (stack.size() > 1) {
            stack.pop();
            refreshImage();
        }
    }

    public void add(Image img){
        stack.add(img);
        refreshImage();
    }

}
