package com.vgvoleg.imageredactor;

import com.vgvoleg.imageredactor.filters.*;
import com.vgvoleg.imageredactor.filters.correction.*;
import com.vgvoleg.imageredactor.filters.matrix.*;
import com.vgvoleg.imageredactor.filters.noisecontrol.*;
import com.vgvoleg.imageredactor.filters.pixel.*;
import com.vgvoleg.imageredactor.filters.pixel.GlassFilter;
import com.vgvoleg.imageredactor.utils.*;
import javafx.application.Application;
import javafx.event.*;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * Created by ovcharuk on 21.09.16.
 */
public class App extends Application {

    // Elements
    private Stage window;
    private ImageManager imageManager;
    private HBox bottomPanel;


    public static void main(String[] args) {
        launch(args);
    }

    private void useFilter(Filter filter, int value, boolean changeable) {
        if (!changeable) {
            BufferedImage invertedImg = Utils.getBufferedImage(imageManager.top());
            invertedImg = filter.processImage(invertedImg, value);
            imageManager.saveTemp();
            imageManager.add(Utils.setImage(invertedImg));
        } else {
            if (imageManager.haveTemp())
                imageManager.pop();
            BufferedImage invertedImg = Utils.getBufferedImage(imageManager.top());
            invertedImg = filter.processImage(invertedImg, value);
            imageManager.setTemp(Utils.setImage(invertedImg));
        }
    }

    private void createBottom(Filter filter, BottomType type) {
        clearBottom();
        Slider slider;  Button applyButton;
        applyButton = new Button("Apply");
        //!!!!!!!!!!!!!!!!!!!!!!!!!!КОСТЫЛЬ РАДИ ОДНОГО ФИЛЬТРА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        switch (type) {
            case SLIDER_BRIGHTNESS:
                slider = new Slider();
                slider.setMax(100);
                slider.setMin(-100);
                slider.setMajorTickUnit(50);
                slider.setShowTickMarks(true);
                slider.adjustValue(0);
                slider.setPrefWidth(400);
                applyButton.setOnAction(e -> {
                    useFilter(filter, (int)slider.getValue(), true);
                });
                bottomPanel.getChildren().addAll(slider, applyButton);
                break;
            case SLIDER_LIGHTNING:
                slider = new Slider();
                slider.setMax(10);
                slider.setMin(1);
                slider.setMajorTickUnit(1);
                slider.setShowTickMarks(true);
                slider.adjustValue(1);
                slider.setPrefWidth(400);
                applyButton.setOnAction(e -> {
                    useFilter(filter, (int)slider.getValue(), true);
                });
                bottomPanel.getChildren().addAll(slider, applyButton);
                break;
            case SLIDER_SEPIA:
                slider = new Slider();
                slider.setMin(0);
                slider.setMax(100);
                slider.setMajorTickUnit(10);
                slider.setShowTickMarks(true);
                slider.adjustValue(0);
                slider.setPrefWidth(400);
                applyButton.setOnAction(e -> {
                    useFilter(filter, (int)slider.getValue(), true);
                });
                bottomPanel.getChildren().addAll(slider, applyButton);
                break;
            case INPUT_COLOR:
                ColorPicker colorPicker = new ColorPicker();
                colorPicker.setOnAction(e->{
                    Color c = colorPicker.getValue();
                    Filter filter1 = new BearingColorFilter((int)(c.getRed()*255), (int)(c.getGreen()*255),(int)(c.getBlue()*255));
                    useFilter(filter1, 0, true);
                });
                bottomPanel.getChildren().addAll(colorPicker);
                break;

        }

    }

    private void clearBottom() {
        if (bottomPanel.getChildren().size() != 0)
        {
            bottomPanel.getChildren().remove(0, bottomPanel.getChildren().size());
        }
    }

    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        primaryStage.setTitle("Image Filters by VGV");
        primaryStage.getIcons().add(new Image("icon.png"));

        BorderPane layout = new BorderPane();
        layout.setTop(createMenu());

        ImageView imageView = new ImageView();
        imageView.setFitWidth(800);
        imageView.setFitHeight(580);
        imageView.setPreserveRatio(true);
        layout.setCenter(imageView);
        imageManager = new ImageManager(imageView);

        bottomPanel = new HBox();
        bottomPanel.setAlignment(Pos.TOP_CENTER);
        layout.setBottom(bottomPanel);
        BorderPane.setMargin(bottomPanel, new Insets(5,3,7,3));

        Scene scene = new Scene(layout, 800, 650);

        primaryStage.setScene(scene);
        primaryStage.show();

    }

    private MenuBar createMenu() {
        MenuBar menuBar = new MenuBar();
        //File
        Menu fileMenu = new Menu("File");
        MenuItem openFile = new MenuItem("Open...");
        MenuItem saveFile = new MenuItem("Save...");
        MenuItem closeFile = new MenuItem("Close");
        MenuItem exitProgram = new MenuItem("Exit");
        fileMenu.getItems().addAll(openFile, saveFile, closeFile,
                new SeparatorMenuItem(),
                exitProgram);


        //Filters
        Menu filterMenu = new Menu("Filters");
        //Pixel
        Menu pixelMenu = new Menu("Pixel");
        MenuItem grayFilter = new MenuItem("Gray");
        MenuItem sepiaFilter = new MenuItem("Sepia");
        MenuItem inversionFilter = new MenuItem("Inversion");
        MenuItem bwFilter = new MenuItem("Black&White");
        MenuItem brightness = new MenuItem("Brightness");
        MenuItem noiseFilter = new MenuItem("Noise");
        MenuItem glassFilter = new MenuItem("Glass");
        pixelMenu.getItems().addAll(inversionFilter, grayFilter,
                sepiaFilter, bwFilter, brightness,
                noiseFilter, glassFilter);
        //Matrix
        Menu matrixFilters = new Menu("Matrix");
        MenuItem blurFilter = new MenuItem("Blur");
        MenuItem gaussianFilter = new MenuItem("Gaussian");
        MenuItem motionBlurFilter = new MenuItem("Motion Blur");
        MenuItem acutanceFilter = new MenuItem("Acutance");
        MenuItem strongAcutanceFilter = new MenuItem("Shakal (Strong Acutance)");
        MenuItem edgeFilter = new MenuItem("Edge");
        MenuItem sobelFilter = new MenuItem("Sobel");
        MenuItem priuttFilter = new MenuItem("Pruitt");
        MenuItem sharrFilter = new MenuItem("Sharr");
        MenuItem stampingFilter = new MenuItem("Stamping");
        MenuItem diffFilter = new MenuItem("Different");
        matrixFilters.getItems().addAll(blurFilter,
                gaussianFilter, motionBlurFilter,
                acutanceFilter, strongAcutanceFilter,
                edgeFilter, sobelFilter, priuttFilter,
                sharrFilter, stampingFilter, diffFilter);

        //Correction
        Menu correctionFilters = new Menu("Correction");
        MenuItem grayWorldFilter = new MenuItem("Gray World");
        MenuItem autolevelsFilter = new MenuItem("Autolevels");
        MenuItem bearingColorFilter = new MenuItem("Coloring");
        MenuItem reflectorFilter = new MenuItem("Perfect Reflector");
        MenuItem staticCorrectionFilter = new MenuItem("Static Correction");
        correctionFilters.getItems().addAll( grayWorldFilter,
                autolevelsFilter, bearingColorFilter, reflectorFilter, staticCorrectionFilter);

        //Noise control
        Menu noiseControlFilters = new Menu("Noise control");
        MenuItem dilationFilter = new MenuItem("Dilation");
        MenuItem erosionFilter = new MenuItem("Erosion");
        MenuItem openingFilter = new MenuItem("Opening");
        MenuItem closingFilter = new MenuItem("Closing");
        noiseControlFilters.getItems().addAll(dilationFilter, erosionFilter,
                openingFilter, closingFilter);


        MenuItem resetMenu = new MenuItem("Reset");

        Menu editMenu = new Menu("Edit");
        MenuItem undoMenu = new MenuItem("Undo");
        editMenu.getItems().addAll(undoMenu, new SeparatorMenuItem(),
                resetMenu);

        filterMenu.getItems().addAll(pixelMenu, matrixFilters, correctionFilters,
                noiseControlFilters);

        menuBar.getMenus().addAll(fileMenu, filterMenu, editMenu);

        //-------------Actions-----------------//
        //-----File-----//
        //Open file
        openFile.setOnAction(e -> {
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose your imageView");

            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"),
                    new FileChooser.ExtensionFilter("BMP", "*.bmp")
            );
            File file = fileChooser.showOpenDialog(null);
            try {
                Image img = Utils.setImage(ImageIO.read(file));
                imageManager.setNewImage(img);
                clearBottom();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

        });
        //Save file
        saveFile.setOnAction(e -> {
            if (imageManager.top() == null){
                showAlert("Error", "Nothing to save", "Please open your image!");
                return;
            }
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose the directory");
            fileChooser.setInitialFileName(".png");
            File file = fileChooser.showSaveDialog(null);
            BufferedImage bufferedImage = Utils.getBufferedImage(imageManager.top());
            try {
                ImageIO.write(bufferedImage, "png", new File(file.getAbsolutePath() + ".png"));
                clearBottom();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });
        //Close file
        closeFile.setOnAction(e->{
            imageManager.setNewImage(null);
        });
        //Exit
        exitProgram.setOnAction(e -> window.close());

        //------Filters------//
        //Inversion
        inversionFilter.setOnAction(new FilterEvent("invert"));
        //Gray
        grayFilter.setOnAction(new FilterEvent("gray"));
        //Sepia
        sepiaFilter.setOnAction(new FilterEvent("sepia"));
        //Black&White
        bwFilter.setOnAction(new FilterEvent("b&w"));
        //Brightness
        brightness.setOnAction(new FilterEvent("bright"));
        //Noise
        noiseFilter.setOnAction(new FilterEvent("noise"));
        //Glass
        glassFilter.setOnAction(new FilterEvent("glass"));
        //Blur
        blurFilter.setOnAction(new FilterEvent("blur"));
        //Gaussian
        gaussianFilter.setOnAction(new FilterEvent("gauss"));
        //Motion Blur
        motionBlurFilter.setOnAction(new FilterEvent("motion"));
        //Acutance
        acutanceFilter.setOnAction(new FilterEvent("acutance"));
        //Strong Acutance
        strongAcutanceFilter.setOnAction(new FilterEvent("acutance2"));
        //Edge
        edgeFilter.setOnAction(new FilterEvent("edge"));
        //Sobel
        sobelFilter.setOnAction(new FilterEvent("sobel"));
        //Pruitt
        priuttFilter.setOnAction(new FilterEvent("pruitt"));
        //Sharr
        sharrFilter.setOnAction(new FilterEvent("sharr"));
        //Stamping
        stampingFilter.setOnAction(new FilterEvent("stamp"));
        //Different
        diffFilter.setOnAction(new FilterEvent("diff"));
        //Gray World
        grayWorldFilter.setOnAction(new FilterEvent("gWorld"));
        //Autolevels
        autolevelsFilter.setOnAction(new FilterEvent("autolvl"));
        //Bearing Color  !!!!!!!!!!!!!!!!!!!!!!!!!!КОСТЫЛЬ РАДИ ОДНОГО ФИЛЬТРА!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        bearingColorFilter.setOnAction(e->{
            if (imageManager.top() == null) {
                showAlert("Error", "There is no image to filter", "Please open your image!");
                return;
            }
            clearBottom();
            createBottom(null, BottomType.INPUT_COLOR);
        });

        //Static correction
        staticCorrectionFilter.setOnAction(e->{
            if (imageManager.top() == null) {
                showAlert("Error", "There is no image to filter", "Please open your image!");
                return;
            }
            StaticCorrectionFilter filter = new StaticCorrectionFilter();

            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Choose your dist image");

            fileChooser.getExtensionFilters().addAll(
                    new FileChooser.ExtensionFilter("JPG", "*.jpg", "*.jpeg"),
                    new FileChooser.ExtensionFilter("PNG", "*.png"),
                    new FileChooser.ExtensionFilter("BMP", "*.bmp")
            );
            File file = fileChooser.showOpenDialog(null);
            try {
                filter.setDistImage(ImageIO.read(file));
                clearBottom();
            } catch (IOException e1) {
                e1.printStackTrace();
            }

            useFilter(filter, 0, true);
        });
        //Perfect Reflector
        reflectorFilter.setOnAction(new FilterEvent("reflector"));
        //Dilation
        dilationFilter.setOnAction(new FilterEvent("dilation"));
        //Erosion
        erosionFilter.setOnAction(new FilterEvent("erosion"));
        //Opening
        openingFilter.setOnAction(new FilterEvent("opening"));
        //Closing
        closingFilter.setOnAction(new FilterEvent("closing"));


        //Undo
        undoMenu.setOnAction(e-> imageManager.pop());
        //Reset
        resetMenu.setOnAction(e -> imageManager.reset());
        return menuBar;
    }

    public void showAlert(String title, String header, String content){
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(header);
        alert.setContentText(content);
        alert.showAndWait();
    }

    private class FilterEvent implements EventHandler<ActionEvent> {
        String nameOfFilter;

        public FilterEvent(String nameOfFilter) {
            this.nameOfFilter = nameOfFilter;
        }

        private Filter getFilterByName(String name) {
            clearBottom();
            switch (name) {
                case "gray":
                    return new GrayFilter();
                case "invert":
                    return new InvertFilter();
                case "sepia":
                    Filter sepiaFilter = new SepiaFilter();
                    createBottom(sepiaFilter, BottomType.SLIDER_SEPIA);
                    return null;
                case "b&w":
                    return new BlackWhiteFilter();
                case "bright":
                    Filter brightnessFilter = new BrightnessFilter();
                    createBottom(brightnessFilter, BottomType.SLIDER_BRIGHTNESS);
                    return null;
                case "noise":
                    return new NoiseFilter();
                case "glass":
                    return new GlassFilter();
                case "blur":
                    return new BlurFilter();
                case "gauss":
                    return new GaussianFilter();
                case "motion":
                    return new MotionBlurFilter();
                case "acutance":
                    return new AcutanceFilter();
                case "acutance2":
                    return new StrongActuanceFilter();
                case "edge":
                    return new EdgeFilter();
                case "sobel":
                    return new SobelFilter();
                case "pruitt":
                    return new PriuttFilter();
                case "sharr":
                    return new SharrFilter();
                case "stamp":
                    return new StampingFilter();
                case "diff":
                    return new DiffFilter(new PriuttFilter(), new SobelFilter(), 100);
                case "gWorld":
                    return new GrayWorldFilter();
                case "autolvl":
                    return new AutolevelsFilter();
                case "reflector":
                    return new PerfectReflectorFilter();
                case "dilation":
                    return new DilationFilter();
                case "erosion":
                    return new ErosionFilter();
                case "opening":
                    return new OpeningFilter();
                case "closing":
                    return new ClosingFilter();
            }
            return null;
        }

        @Override
        public void handle(ActionEvent event) {
            if (imageManager.top() == null) {
                showAlert("Error", "There is no image to filter", "Please open your image!");
                return;
            }
            if (getFilterByName(nameOfFilter) == null){
                return;
            }
            useFilter(getFilterByName(nameOfFilter), 0, false);
        }
    }
}
