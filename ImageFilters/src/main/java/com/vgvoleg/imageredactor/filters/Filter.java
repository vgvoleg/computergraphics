package com.vgvoleg.imageredactor.filters;

import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 21.09.16.
 */
public abstract class Filter {

    protected int value = 0;

    public BufferedImage processImage(BufferedImage sourceImage, int value){
        this.value = value;
        BufferedImage resultImage = new BufferedImage(sourceImage.getWidth(),
                sourceImage.getHeight(),
                sourceImage.getType());
        for (int i = 0; i<sourceImage.getWidth(); i++)
            for (int j = 0; j<sourceImage.getHeight(); j++){
                resultImage.setRGB(i, j, calculateNewPixelColor(sourceImage, i, j));
            }
        return resultImage;
    }

    protected abstract int calculateNewPixelColor(BufferedImage sourceImage, int i, int j);

    public int clamp(int value, int min, int max){
        if (value< min){
            return min;
        } else if (value > max){
            return max;
        } else {
            return value;
        }
    }

}
