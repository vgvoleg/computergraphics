package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 21.09.2016.
 */
public class BlackWhiteFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i,j));
        int newColor = (sourceColor.getRed()
                + sourceColor.getGreen()
                + sourceColor.getBlue())/3;
        newColor = newColor>127 ? 255: 0;
        Color resultColor = new Color(newColor, newColor, newColor);
        return resultColor.getRGB();
    }
}
