package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class SobelFilter extends DoubleMatrixFilter {

    public SobelFilter() {
        kernelX = new float[][] {
                {-1, 0, 1},
                {-2, 0, 2},
                {-1, 0, 1}
        };

        kernelY = new float[][] {
                {-1, -2, -1},
                {0, 0, 0},
                {1, 2, 1}
        };
    }


}
