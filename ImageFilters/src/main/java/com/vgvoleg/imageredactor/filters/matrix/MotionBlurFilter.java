package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 23.09.16.
 */
public class MotionBlurFilter extends MatrixFilter {
    public MotionBlurFilter() {
        int n = 5;
        kernel = new float[n][n];
        for (int i = 0; i<n; i++)
            for (int j = 0; j<n; j++){
                if (i == j){
                    kernel[i][j] = 1.0f/n;
                } else {
                    kernel[i][j] = 0;
                }
            }
    }
}
