package com.vgvoleg.imageredactor.filters.matrix;

import com.vgvoleg.imageredactor.utils.Utils;
import com.vgvoleg.imageredactor.filters.Filter;
import javafx.scene.image.Image;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 28.09.16.
 */
public class DiffFilter extends MatrixFilter {

    private BufferedImage firstImage;
    private BufferedImage secondImage;

    Filter firstFilter, secondFilter;

    public DiffFilter(Filter firstFilter, Filter secondFilter,  int value){
        this.firstFilter = firstFilter;
        this.secondFilter = secondFilter;
        this.value = value;
    }


    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        Image tmp = Utils.setImage(sourceImage);
        firstImage = Utils.getBufferedImage(tmp);
        secondImage = Utils.getBufferedImage(tmp);

        firstImage = firstFilter.processImage(firstImage, 0);
        secondImage = secondFilter.processImage(secondImage, 0);

        return calculateDifferent(firstImage, secondImage);
    }


    private BufferedImage calculateDifferent(BufferedImage first, BufferedImage second){
        BufferedImage resultImage = new BufferedImage(first.getWidth(), first.getHeight(),
                first.getType());

        int resultRed, resultGreen,resultBlue;

        for (int i = 0; i< resultImage.getWidth(); i++)
            for (int j = 0; j< resultImage.getHeight(); j++){
                Color firstColor = new Color(first.getRGB(i, j));
                Color secondColor = new Color(second.getRGB(i, j));

                resultRed = clamp((firstColor.getRed() - secondColor.getRed())*value, 0, 255);

                resultGreen = clamp((firstColor.getGreen() - secondColor.getGreen())*value, 0, 255);

                resultBlue = clamp((firstColor.getBlue() - secondColor.getBlue())*value, 0, 255);

                resultImage.setRGB(i, j, new Color(resultRed, resultGreen, resultBlue).getRGB());
            }

        return resultImage;
    }
}
