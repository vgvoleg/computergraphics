package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class StrongActuanceFilter extends MatrixFilter {
    public StrongActuanceFilter() {
        kernel = new float[][] {
                {-1, -1, -1},
                {-1, 9, -1},
                {-1, -1, -1}
        };
    }
}
