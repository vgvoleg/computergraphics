package com.vgvoleg.imageredactor.filters.correction;

import com.vgvoleg.imageredactor.filters.Filter;
import com.vgvoleg.imageredactor.utils.*;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 05.10.16.
 */
public class StaticCorrectionFilter extends Filter {

    private double redSrcMO = 0.0, greenSrcMO = 0.0, blueSrcMO = 0.0;
    private double redDistMO = 0.0, greenDistMO = 0.0, blueDistMO = 0.0;

    private double redSrcDISP = 0.0, greenSrcDISP = 0.0, blueSrcDISP = 0.0;
    private double redDistDISP = 0.0, greenDistDISP = 0.0, blueDistDISP = 0.0;

    private BufferedImage distImage;

    public void setDistImage(BufferedImage distImage){
        this.distImage = distImage;
    }

    private void doSomeTVIMSShit(BufferedImage sourceImage, BufferedImage distImage){
        //Мат ожидание для первого изображения
        int n1 = sourceImage.getWidth()*sourceImage.getHeight();
        for (int i = 0; i< sourceImage.getWidth(); i++){
            for (int j=0; j<sourceImage.getHeight(); j++){
                Color color = new Color(sourceImage.getRGB(i,j));
                double[][] colorLab = MatrixOp.convertToLab(color.getRed(), color.getGreen(), color.getBlue());
                redSrcMO += colorLab[0][0];
                greenSrcMO += colorLab[1][0];
                blueSrcMO += colorLab[2][0];
            }
        }

        redSrcMO/=n1;
        greenSrcMO/=n1;
        blueSrcMO/=n1;

        //Мат ожидание для второго изображения
        int n2 = distImage.getWidth()*distImage.getHeight();
        for (int i = 0; i< distImage.getWidth(); i++){
            for (int j=0; j<distImage.getHeight(); j++){
                Color color = new Color(distImage.getRGB(i,j));
                double[][] colorLab = MatrixOp.convertToLab(color.getRed(), color.getGreen(), color.getBlue());
                redDistMO += colorLab[0][0];
                greenDistMO += colorLab[1][0];
                blueDistMO += colorLab[2][0];
            }
        }
        redDistMO/=n2;
        greenDistMO/=n2;
        blueDistMO/=n2;

        //Дисперсия для первого изображения
        for (int i = 0; i< sourceImage.getWidth(); i++){
            for (int j=0; j<sourceImage.getHeight(); j++){
                Color color = new Color(sourceImage.getRGB(i,j));
                double[][] colorLab = MatrixOp.convertToLab(color.getRed(), color.getGreen(), color.getBlue());
                redSrcDISP += Math.pow(colorLab[0][0] - redSrcMO, 2);
                greenSrcDISP += Math.pow(colorLab[1][0] - greenSrcMO, 2);
                blueSrcDISP += Math.pow(colorLab[2][0] - blueSrcMO, 2);
            }
        }
        redSrcDISP = Math.sqrt(redSrcDISP/n1);
        greenSrcDISP = Math.sqrt(greenSrcDISP/n1);
        blueSrcDISP = Math.sqrt(blueSrcDISP/n1);

        //Дисперсия для второго изображения
        for (int i = 0; i< distImage.getWidth(); i++){
            for (int j=0; j<distImage.getHeight(); j++){
                Color color = new Color(distImage.getRGB(i,j));
                double[][] colorLab = MatrixOp.convertToLab(color.getRed(), color.getGreen(), color.getBlue());
                redDistDISP += Math.pow(colorLab[0][0] - redDistMO, 2);
                greenDistDISP += Math.pow(colorLab[1][0] - greenDistMO,2);
                blueDistDISP += Math.pow(colorLab[2][0] - blueDistMO,2);
            }
        }
        redDistDISP = Math.sqrt(redDistDISP/n2);
        greenDistDISP = Math.sqrt(greenDistDISP/n2);
        blueDistDISP = Math.sqrt(blueDistDISP/n2);

    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        doSomeTVIMSShit(sourceImage, distImage);
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j) {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        double[][] colorLab = MatrixOp.convertToLab(sourceColor.getRed(), sourceColor.getGreen(), sourceColor.getBlue());

        double newRed = redDistMO + (colorLab[0][0] - redSrcMO)*
                redDistDISP/redSrcDISP;

        double newGreen = greenDistMO + (colorLab[1][0] - greenSrcMO)*
                greenDistDISP/greenSrcDISP;

        double newBlue = blueDistMO + (colorLab[2][0] - blueSrcMO)*
                blueDistDISP/blueSrcDISP;

        int[][] newColor = MatrixOp.convertToRGB(new double[][]{
                {newRed},
                {newGreen},
                {newBlue}
        });

        return new Color(clamp(newColor[0][0], 0, 255), clamp(newColor[1][0],0,255), clamp(newColor[2][0], 0, 255)).getRGB();
    }
}
