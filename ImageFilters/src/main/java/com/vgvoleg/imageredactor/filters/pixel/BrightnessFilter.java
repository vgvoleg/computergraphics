package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.utils.Utils;
import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 21.09.2016.
 */
public class BrightnessFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i,j));
        int red = Utils.getCorrectColorValue(sourceColor.getRed() + value);
        int green = Utils.getCorrectColorValue(sourceColor.getGreen() + value);
        int blue = Utils.getCorrectColorValue(sourceColor.getBlue() + value);
        Color resultColor = new Color(red, green, blue);
        return resultColor.getRGB();
    }
}
