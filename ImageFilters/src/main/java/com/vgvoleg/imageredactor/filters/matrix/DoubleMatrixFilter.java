package com.vgvoleg.imageredactor.filters.matrix;

import javafx.embed.swing.SwingFXUtils;
import javafx.scene.image.Image;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 22.09.16.
 */
public abstract class DoubleMatrixFilter extends MatrixFilter {

    private BufferedImage firstImage;
    private BufferedImage secondImage;
    protected float[][] kernelX;
    protected float[][] kernelY;

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        Image tmp = SwingFXUtils.toFXImage(sourceImage, null);
        firstImage = SwingFXUtils.fromFXImage(tmp, null);
        secondImage = SwingFXUtils.fromFXImage(tmp, null);

        setKernel(kernelX);
        firstImage = super.processImage(firstImage, value);

        setKernel(kernelY);
        secondImage = super.processImage(secondImage, value);


        return calculateSumMatrix(firstImage, secondImage);
    }

    private BufferedImage calculateSumMatrix(BufferedImage first, BufferedImage second){
        BufferedImage resultImage = new BufferedImage(first.getWidth(), first.getHeight(),
                first.getType());

        int resultRed, resultGreen,resultBlue;

        for (int i = 0; i< resultImage.getWidth(); i++)
            for (int j = 0; j< resultImage.getHeight(); j++){
                Color firstColor = new Color(first.getRGB(i, j));
                Color secondColor = new Color(second.getRGB(i, j));

                resultRed = clamp((int)Math.sqrt(firstColor.getRed()*firstColor.getRed()
                                + secondColor.getRed()*secondColor.getRed()), 0, 255);

                resultGreen = clamp((int)Math.sqrt(firstColor.getGreen()*firstColor.getGreen()
                                + secondColor.getGreen()*secondColor.getGreen()), 0, 255);

                resultBlue = clamp((int)Math.sqrt(firstColor.getBlue()*firstColor.getBlue()
                                + secondColor.getBlue()*secondColor.getBlue()), 0, 255);

                resultImage.setRGB(i, j, new Color(resultRed, resultGreen, resultBlue).getRGB());
            }

        return resultImage;
    }
}
