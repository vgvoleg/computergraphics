package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 21.09.16.
 */
public class InvertFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j) {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        Color invertColor = new Color(255 - sourceColor.getRed(),
                255 - sourceColor.getGreen(),
                255 - sourceColor.getBlue());
        return invertColor.getRGB();
    }
}
