package com.vgvoleg.imageredactor.filters.noisecontrol;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class ErosionFilter extends Filter
{
    private float[][] mask = new float[][] {
            {0, 1, 0},
            {1, 1, 1},
            {0, 1, 0}
    };


    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value)
    {
        int width = sourceImage.getWidth();
        int height = sourceImage.getHeight();

        int MW = mask.length;
        int MH = mask[0].length;

        BufferedImage resultImage = new BufferedImage(width, height, sourceImage.getType());

        for(int y = MH/2; y < (height - MH/2); y++)
        {
            for (int x = MW / 2; x < (width - MW / 2); x++)
            {
                int min = Integer.MAX_VALUE;

                for (int j = -MH / 2; j <= (MH / 2); j++)

                    for (int i = -MW / 2; i <= (MW / 2); i++)
                    {
                        if ((mask[1 + i][1 + j] == 1) && (sourceImage.getRGB(x + i, y + j) < min))
                        {
                            min = sourceImage.getRGB(x + i, y + j);
                        }
                    }
                resultImage.setRGB(x, y, min);
            }
        }

        return resultImage;
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        return 0;
    }
}
