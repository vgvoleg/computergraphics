package com.vgvoleg.imageredactor.filters.correction;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class PerfectReflectorFilter extends Filter
{
    private int redMax, greenMax, blueMax;

    private void calculateMax(BufferedImage sourceImage){
        redMax = greenMax = blueMax = 0;
        for (int i = 0; i<sourceImage.getWidth(); i++)
            for (int j = 0; j<sourceImage.getHeight(); j++){
                Color color = new Color(sourceImage.getRGB(i, j));

                if (color.getRed() > redMax)
                    redMax = color.getRed();

                if (color.getGreen() > greenMax)
                    greenMax = color.getGreen();

                if (color.getBlue() > blueMax)
                    blueMax = color.getBlue();
            }
    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value)
    {
        calculateMax(sourceImage);
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i,j));
        int newRed = sourceColor.getRed()*255/redMax;
        int newGreen = sourceColor.getGreen()*255/greenMax;
        int newBlue = sourceColor.getBlue()*255/blueMax;

        return new Color(newRed, newGreen, newBlue).getRGB();
    }
}
