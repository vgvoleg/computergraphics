package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class AcutanceFilter extends MatrixFilter {
    public AcutanceFilter() {
        kernel = new float[][] {
                {0, -1, 0},
                {-1, 5, -1},
                {0, -1, 0}
        };

    }
}
