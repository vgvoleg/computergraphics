package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 21.09.16.
 */
public class BlurFilter extends MatrixFilter {

    public BlurFilter() {
        int sizeX = 3;
        int sizeY = 3;
        kernel = new float[sizeX][sizeY];
        for (int i = 0; i<sizeX; i++)
            for (int j = 0; j<sizeY; j++)
                kernel[i][j] = 1.0f / (float)(sizeX*sizeY);
    }
}
