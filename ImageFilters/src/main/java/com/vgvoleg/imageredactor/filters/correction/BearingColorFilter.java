package com.vgvoleg.imageredactor.filters.correction;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 04.10.2016.
 */
public class BearingColorFilter extends Filter
{
    private int redResult = 128;
    private int greenResult = 128;
    private int blueResult = 128;

    private int redSource;
    private int greenSource;
    private int blueSource;

    public BearingColorFilter(int redSource, int greenSource, int blueSource)
    {
        this.redSource = redSource;
        this.greenSource = greenSource;
        this.blueSource = blueSource;
    }

    public BufferedImage processImage(BufferedImage sourceImage, int value)
    {
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        int newRed = sourceColor.getRed()*redSource/redResult;
        int newGreen = sourceColor.getGreen()*greenSource/greenResult;
        int newBlue = sourceColor.getBlue()*blueSource/blueResult;

        return new Color(clamp(newRed, 0 , 255), clamp(newGreen,0,255), clamp(newBlue, 0, 255)).getRGB();
    }
}
