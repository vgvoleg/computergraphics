package com.vgvoleg.imageredactor.filters.noisecontrol;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.image.BufferedImage;

/**
 * Created by Олег on 03.10.2016.
 */
public class OpeningFilter extends Filter {
    private Filter dilationFilter;
    private Filter erosionFilter;

    public OpeningFilter() {
        dilationFilter = new DilationFilter();
        erosionFilter = new ErosionFilter();
    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        BufferedImage result = erosionFilter.processImage(sourceImage, value);
        result = dilationFilter.processImage(result, value);
        return result;
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j) {
        return 0;
    }
}
