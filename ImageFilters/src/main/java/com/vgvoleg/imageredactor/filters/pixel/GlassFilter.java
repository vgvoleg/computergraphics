package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class GlassFilter extends Filter
{

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        int x = (int)(i + (Math.random()+0.5)*10);
        int y = (int)(j + (Math.random()+0.5)*10);
        Color sourceColor;

        if (x < sourceImage.getWidth() && y < sourceImage.getHeight())
        {
            sourceColor = new Color(sourceImage.getRGB(x, y));
        } else {
            sourceColor = new Color(sourceImage.getRGB(i, j));
        }

        return sourceColor.getRGB();
    }
}
