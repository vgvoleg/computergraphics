package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class PriuttFilter extends DoubleMatrixFilter {

    public PriuttFilter(){
        kernelX = new float[][] {
                {-1, 0, 1},
                {-1, 0, 1},
                {-1, 0, 1}
        };

        kernelY = new float[][] {
                {-1, -1, -1},
                {0, 0, 0},
                {1, 1, 1}
        };
    }

}
