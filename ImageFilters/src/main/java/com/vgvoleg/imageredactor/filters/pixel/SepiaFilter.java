package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.utils.Utils;
import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 21.09.2016.
 */
public class SepiaFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));

        int middle = (sourceColor.getRed() + sourceColor.getGreen() + sourceColor.getGreen())/3;
        Color grayColor = new Color(Utils.getCorrectColorValue(middle + value*2),
                Utils.getCorrectColorValue((int)(middle + 0.5*value)),
                Utils.getCorrectColorValue(middle - value));
        return grayColor.getRGB();
    }
}
