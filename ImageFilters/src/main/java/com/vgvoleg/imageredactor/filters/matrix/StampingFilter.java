package com.vgvoleg.imageredactor.filters.matrix;

import com.vgvoleg.imageredactor.filters.Filter;
import com.vgvoleg.imageredactor.filters.pixel.GrayFilter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class StampingFilter extends MatrixFilter {
    public StampingFilter() {
        kernel = new float[][]{
                {0, 1, 0},
                {1, 0, -1},
                {0, -1, 0}
        };
    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        Filter firstFilter = new GrayFilter();
        sourceImage = firstFilter.processImage(sourceImage, value);
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j) {
        int radiusX = kernel.length / 2;
        int radiusY = kernel[0].length / 2;

        float resultRed = 0, resultGreen = 0, resultBlue = 0;

        for (int l = -radiusY; l <= radiusY; l++)
            for (int k = -radiusX; k<= radiusX; k++){
                int idX = clamp(i + k, 0, sourceImage.getWidth()-1);
                int idY = clamp(j + l, 0, sourceImage.getHeight()-1);
                Color neighborColor = new Color(sourceImage.getRGB(idX,idY));

                resultRed += neighborColor.getRed() *
                        kernel[k + radiusX][l + radiusY];
                resultGreen += neighborColor.getGreen() *
                        kernel[k + radiusX][l + radiusY];
                resultBlue += neighborColor.getBlue() *
                        kernel[k + radiusX][l + radiusY];
            }

        return new Color(clamp((int)resultRed + 128,0,255),
                clamp((int)resultGreen + 128, 0, 255),
                clamp((int)resultBlue + 128,0,255))
                .getRGB();
    }
}
