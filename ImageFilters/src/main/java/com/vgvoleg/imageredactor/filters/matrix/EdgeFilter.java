package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 05.10.16.
 */
public class EdgeFilter extends MatrixFilter {
    public EdgeFilter() {
        kernel = new float[][] {
                {-1, -1, -1},
                {-1, 8, -1},
                {-1, -1, -1}
        };
    }
}
