package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 21.09.2016.
 */
public class GrayFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        int gray = (int)(0.36*sourceColor.getRed() + 0.53*sourceColor.getGreen() + 0.11*sourceColor.getGreen());
        Color grayColor = new Color(gray, gray, gray);
        return grayColor.getRGB();
    }
}
