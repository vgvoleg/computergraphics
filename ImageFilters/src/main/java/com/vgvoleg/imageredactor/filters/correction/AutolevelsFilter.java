package com.vgvoleg.imageredactor.filters.correction;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class AutolevelsFilter extends Filter {
    int redMin, greenMin, blueMin;
    int redMax, greenMax, blueMax;

    private void calculateMinMax(BufferedImage sourceImage) {
        redMin = greenMin = blueMin = 255;
        redMax = greenMax = blueMax = 0;
        for (int i = 0; i < sourceImage.getWidth(); i++)
            for (int j = 0; j < sourceImage.getHeight(); j++) {
                Color color = new Color(sourceImage.getRGB(i, j));

                if (color.getRed() > redMax)
                    redMax = color.getRed();
                if (color.getRed() < redMin)
                    redMin = color.getRed();

                if (color.getGreen() > greenMax)
                    greenMax = color.getGreen();
                if (color.getGreen() < greenMin)
                    greenMin = color.getGreen();

                if (color.getBlue() > blueMax)
                    blueMax = color.getBlue();
                if (color.getBlue() < blueMin)
                    blueMin = color.getBlue();

            }
    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value) {
        calculateMinMax(sourceImage);
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j) {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        int newRed = (sourceColor.getRed() - redMin) * 255 / (redMax - redMin);
        int newGreen = (sourceColor.getGreen() - greenMin) * 255 / (greenMax - greenMin);
        int newBlue = (sourceColor.getBlue() - blueMin) * 255 / (blueMax - blueMin);

        return new Color(newRed, newGreen, newBlue).getRGB();
    }
}
