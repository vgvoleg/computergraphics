package com.vgvoleg.imageredactor.filters.pixel;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Random;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class NoiseFilter extends Filter
{
    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        value = 100;
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        Random random = new Random();

        int randRed = value - random.nextInt(2*value);
        int randGreen = value - random.nextInt(2*value);
        int randBlue = value - random.nextInt(2*value);

        return new Color(clamp(sourceColor.getRed() + randRed, 0 , 255),
                clamp(sourceColor.getGreen() + randGreen, 0 , 255),
                clamp(sourceColor.getBlue() + randBlue, 0 , 255))
                .getRGB();
    }
}
