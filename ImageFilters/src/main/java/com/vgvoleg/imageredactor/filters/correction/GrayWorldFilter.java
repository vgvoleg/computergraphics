package com.vgvoleg.imageredactor.filters.correction;

import com.vgvoleg.imageredactor.filters.Filter;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * Created by olov0816 on 03.10.2016.
 */
public class GrayWorldFilter extends Filter
{

    private int average;

    private int averRed, averGreen, averBlue;

    private void calculateAverage(BufferedImage sourceImage){
        int sumRed, sumGreen, sumBlue;
        sumRed = sumGreen = sumBlue = 0;
        int N = sourceImage.getWidth()*sourceImage.getHeight();

        for (int i = 0; i<sourceImage.getWidth(); i++)
            for (int j = 0; j<sourceImage.getHeight(); j++){
                Color color = new Color(sourceImage.getRGB(i, j));
                sumRed+= color.getRed();
                sumGreen+= color.getGreen();
                sumBlue+= color.getBlue();
            }

        averRed = sumRed/N;
        averGreen = sumGreen/N;
        averBlue = sumBlue/N;

        average = (averRed + averGreen + averBlue)/3;
    }

    @Override
    public BufferedImage processImage(BufferedImage sourceImage, int value)
    {
        calculateAverage(sourceImage);
        return super.processImage(sourceImage, value);
    }

    @Override
    protected int calculateNewPixelColor(BufferedImage sourceImage, int i, int j)
    {
        Color sourceColor = new Color(sourceImage.getRGB(i, j));
        int newRed = sourceColor.getRed()*average/averRed;
        int newGreen = sourceColor.getGreen()*average/averGreen;
        int newBlue = sourceColor.getBlue()*average/averBlue;

        return new Color(clamp(newRed, 0, 255), clamp(newGreen,0,255), clamp(newBlue, 0, 255)).getRGB();
    }
}
