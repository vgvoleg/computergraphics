package com.vgvoleg.imageredactor.filters.matrix;

/**
 * Created by ovcharuk on 22.09.16.
 */
public class SharrFilter extends DoubleMatrixFilter {

    public SharrFilter(){
        kernelX = new float[][] {
                {3, 0, -3},
                {10, 0, -10},
                {3, 0, -3}
        };

        kernelY = new float[][] {
                {3, 10, 3},
                {0, 0, 0},
                {-3, -10, -3}
        };
    }
}
