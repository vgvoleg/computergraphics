package com.vgvoleg.opengl;

import com.google.common.io.LittleEndianDataInputStream;

import java.io.*;

/**
 * Created by ovcharuk on 20.10.16.
 */
public class Bin {
    public static int X, Y, Z;
    public static short[] array;
    public Bin(){
    }

//    public static short swap (short value)
//    {
//        int b1 = value & 0xff;
//        int b2 = (value >> 8) & 0xff;
//
//        return (short) (b1 << 8 | b2 << 0);
//    }

//    public static int swap (int value)
//    {
//        int b1 = (value >>  0) & 0xff;
//        int b2 = (value >>  8) & 0xff;
//        int b3 = (value >> 16) & 0xff;
//        int b4 = (value >> 24) & 0xff;
//
//        return b1 << 24 | b2 << 16 | b3 << 8 | b4 << 0;
//    }

    public void readBin(String filePath){
        try {
            File file = new File(filePath);
            LittleEndianDataInputStream stream = new LittleEndianDataInputStream(new FileInputStream(file));

            //RandomAccessFile stream = new RandomAccessFile(file, "r");
            X = stream.readInt();
            Y = stream.readInt();
            Z = stream.readInt();


            int arraySize = X*Y*Z;
            array = new short[arraySize];
            for (int i = 0; i<array.length; i++){
                array[i] = stream.readShort();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
