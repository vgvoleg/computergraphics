package com.vgvoleg.opengl;

import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;

import java.awt.*;

import static com.jogamp.opengl.GL.*;
import static com.jogamp.opengl.GL2GL3.GL_QUADS;
import static com.jogamp.opengl.fixedfunc.GLLightingFunc.GL_SMOOTH;
import static com.jogamp.opengl.fixedfunc.GLMatrixFunc.GL_PROJECTION;

/**
 * Created by ovcharuk on 19.10.16.
 */
public class GLGraphics implements GLEventListener {

    private GLU glu;
    private int layers = 0;
    private int min = 0;
    private int max = 2000;
    private boolean ready = false;

    @Override
    public void init(GLAutoDrawable glAutoDrawable) {
        glu = new GLU();                         // get GL Utilities
        Bin bin = new Bin();
        bin.readBin("testdata.bin");
        ready = true;
    }

    @Override
    public void dispose(GLAutoDrawable glAutoDrawable) {
    }

    @Override
    public void display(GLAutoDrawable glAutoDrawable) {
        GL2 gl = glAutoDrawable.getGL().getGL2();  // get the OpenGL 2 graphics context
        gl.glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        if (ready) {
            drawQuads(gl, layers);
            glAutoDrawable.swapBuffers();
        }
    }

    @Override
    public void reshape(GLAutoDrawable glAutoDrawable, int x, int y, int width, int height) {
        GL2 gl = glAutoDrawable.getGL().getGL2();  // get the OpenGL 2 graphics context

        if (height == 0) height = 1;   // prevent divide by zero

        gl.glShadeModel(GL_SMOOTH);
        gl.glMatrixMode(GL_PROJECTION);
        gl.glLoadIdentity();

        gl.glOrtho(0, Bin.X, 0, Bin.Y, -1, 1);

        gl.glViewport(0, 0, width, height);
    }

    private float transferFunction(short value){
        float newVal = clamp(255.0f*(value - min)/(max-min), 0, 255);
        return newVal;
    }

    private float clamp(float value, int min, int max){
        if (value<min){
            value = min;
        }
        if (value>max){
            value = max;
        }
        return value;
    }

    private void drawQuads(GL2 gl, int layerNumber){
        gl.glBegin(GL_QUADS);
        short value;
        float c;
        for (int x_coord = 0; x_coord<Bin.X - 1; x_coord++){
            for (int y_coord = 0; y_coord<Bin.Y - 1; y_coord++){
                // 1
                value = Bin.array[x_coord+y_coord*Bin.X + layerNumber*Bin.X*Bin.Y];
                c = transferFunction(value)/255.0f;
                gl.glColor3f(c, c, c);
                gl.glVertex2i(x_coord, y_coord);

                // 2
                value = Bin.array[x_coord+(y_coord + 1)*Bin.X + layerNumber*Bin.X*Bin.Y];
                c = transferFunction(value)/255.0f;
                gl.glColor3f(c, c, c);
                gl.glVertex2i(x_coord, y_coord + 1);

                // 3
                value = Bin.array[(x_coord + 1) +(y_coord + 1)*Bin.X + layerNumber*Bin.X*Bin.Y];
                c = transferFunction(value)/255.0f;
                gl.glColor3f(c, c, c);
                gl.glVertex2i(x_coord + 1, y_coord + 1);

                // 4
                value = Bin.array[(x_coord +1)+y_coord*Bin.X + layerNumber*Bin.X*Bin.Y];
                c = transferFunction(value)/255.0f;
                gl.glColor3f(c, c, c);
                gl.glVertex2i(x_coord + 1, y_coord);
            }
        }
        gl.glEnd();
    }

    public void incLayers(){
        if (layers<Bin.Z-1)
        layers++;
    }

    public void decLayers(){
        if (layers>0) {
            layers--;
        }
    }

    public void incMin(){
        if(min<max - 100){
            min+=100;
        }
    }
    public void decMin(){
        if (min>-3000){
            min-=100;
        }
    }

    public void incMax(){
        if (max<5000){
            max+=100;
        }
    }
    public void decMax(){
        if (max>min + 100){
            max-=100;
        }
    }
}
