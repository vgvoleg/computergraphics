package com.vgvoleg.opengl;


import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by ovcharuk on 26.10.16.
 */
public class MainFrame extends JFrame implements KeyListener {

    private static GLGraphics renderer;

    public MainFrame(){
        addKeyListener(this);
    }

    public static void main(String[] args) {
        MainFrame frame = new MainFrame();
        frame.setTitle("privet");
        final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);

        // The canvas
        final GLCanvas glcanvas = new GLCanvas(capabilities);
        renderer = new GLGraphics();
        glcanvas.addGLEventListener(renderer);
        glcanvas.setSize(600, 600);

        final FPSAnimator animator = new FPSAnimator(glcanvas, 300, true);

        frame.getContentPane().add(glcanvas);

        //Shutdown
        frame.addWindowListener(new WindowAdapter(){
            public void windowClosing(WindowEvent e){
                if(animator.isStarted())
                    animator.stop();
                System.exit(0);
            }
        });
        frame.setSize(frame.getContentPane().getPreferredSize());

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        int windowX = Math.max(0, (screenSize.width - frame.getWidth()) / 2);
        int windowY = Math.max(0, (screenSize.height - frame.getHeight()) / 2);

        frame.setLocation(windowX, windowY);

        frame.setVisible(true);

        animator.start();
    }


    @Override
    public void keyTyped(KeyEvent e) {
    }

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()){
            case KeyEvent.VK_UP:
                renderer.incLayers();
                break;
            case KeyEvent.VK_DOWN:
                renderer.decLayers();
                break;
            case KeyEvent.VK_F1:
                renderer.decMin();
                break;
            case KeyEvent.VK_F2:
                renderer.incMin();
                break;
            case KeyEvent.VK_F3:
                renderer.decMax();
                break;
            case KeyEvent.VK_F4:
                renderer.incMax();
                break;
        }
    }

    @Override
    public void keyReleased(KeyEvent e) {
    }
}
